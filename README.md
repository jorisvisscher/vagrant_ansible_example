## Synopsis

Quick example which uses an ansible playbook to install ZIP in a Centos host running in Vagrant.

## Code Example

```sh
$ vagrant up
```

^^ Should do the trick

## License

[MIT license](LICENSE)